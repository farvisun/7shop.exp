<?php
// PATH
define('ABSPATH',__DIR__."/../");
define('UPLOAD_PATH', ABSPATH . 'upload/');
define('VIEWS',ABSPATH.'views/');
define('ROUTES',ABSPATH.'routes/');
define('CONFIG',ABSPATH.'config/');

// URLs
define('BASE_URL','http://7shop.exp/');
define('UPLOAD_BASE_URL', BASE_URL . 'upload/');
