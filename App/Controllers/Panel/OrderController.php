<?php
namespace App\Controllers\Panel;

use App\Services\View\View;

class OrderController{

    public function index($request)
    {
        $data = [];
        View::load('panel.orders.index', $data, 'panel-admin');
    }

    public function payments($request)
    {
        $data = [];
        View::load('panel.orders.payments', $data, 'panel-admin');
    }

    public function shipments($request)
    {
        $data = [];
        View::load('panel.orders.shipments', $data, 'panel-admin');
    }


}