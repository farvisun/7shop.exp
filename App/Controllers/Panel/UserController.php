<?php
namespace App\Controllers\Panel;

use App\Core\Request;
use App\Repositories\UserRepo;
use App\Services\Flash\FlashMessage;
use App\Services\View\View;

class UserController{

    public function index($request)
    {
        $data = [];
        View::load('panel.user.index', $data, 'panel-admin');
    }

    public function add($request)
    {
        $data = [];
        View::load('panel.user.add', $data, 'panel-admin');
    }

    public function create($request)
    {
        // input validation & filtering here
        // check if user exists !
        $vRes = true;   // result of validation and filtering
        if ($vRes === true) {
            $repo = new UserRepo();
            $repo->create($request->except(['csrf']));
            FlashMessage::add("انجام شد", FlashMessage::SUCCESS);
        }
        Request::redirect('panel/users/add');
    }

    public function profile($request)
    {
        $data = [];
        View::load('panel.user.profile', $data, 'panel-admin');
    }

    public function addresses($request)
    {
        $data = [];
        View::load('panel.user.addresses', $data, 'panel-admin');
    }


}