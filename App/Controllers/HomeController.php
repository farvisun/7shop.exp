<?php

namespace App\Controllers;

use App\Core\Request;
use App\Repositories\CategoryRepo;
use App\Repositories\ProductRepo;
use App\Services\View\View;

class HomeController
{

    public function index(Request $request)
    {
        $productRepo = new ProductRepo();
        $categoryRepo = new CategoryRepo();

        $slides = $productRepo->latestWithSlide(3) ;

        $randomCategories = $categoryRepo->random(3);

        $latest = $productRepo->latest(8);
        $randomProducts = $productRepo->random(1);
        $bestSeller = $productRepo->bestSeller(8) ;

        $data = [
            'slides' => $slides,
            'randomCategories' => $randomCategories,
            'latest' => $latest,
            'bestSeller' => $bestSeller,
            'randomProducts' => $randomProducts,
        ];

        View::load('home.index', $data);
    }

}