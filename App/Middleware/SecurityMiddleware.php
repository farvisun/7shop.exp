<?php
namespace App\Middleware;

class SecurityMiddleware extends BaseMiddleware {
	function handle($request) {
        if(!empty($request->param('csrf')) and $request->param('csrf') == $_SESSION['csrf_token'] ){
            return $request;
        }else{
            echo "CSRF Token Mismatch";
            die();
        }
	}
}