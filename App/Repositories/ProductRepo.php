<?php
namespace App\Repositories;
use App\Models\Product;

class ProductRepo extends BaseRepo {
    protected $model = Product::class;
    public function setAttributes($product, $attibutes)
    {
        $attrValRepo = new AttributeValueRepo();
        foreach ($attibutes as $attr_id=>$attr_value){
            $attrValRepo->create([
                'category_id'=> $product->category_id,
                'product_id'=> $product->id,
                'attribute_id'=> $attr_id,
                'value'=>$attr_value
            ]);
        }
    }

    public function latest($num)
    {
        return $this->model::orderBy('create_at', 'desc')->take($num)->get();
    }
    public function bestSeller($num,$days = 7)
    {
        // this is your Work !!!
        // bestSeller : products with highest Order Count !
        return $this->latest($num);
    }


    public function latestWithSlide($num)
    {
        return $this->model::join('media', 'media.product_id', '=', 'products.id')
            ->where('media.type', '=', 'product_slide')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->orderBy('media.created_at', 'desc')
            ->select('categories.title as slug','categories.title as category', 'media.url as img', 'products.id as pid', 'products.title as title')
            ->take($num)->get();
    }

}