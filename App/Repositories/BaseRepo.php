<?php
namespace App\Repositories;

abstract class BaseRepo{
    protected $model;

    public function create(array $data)
    {
        return $this->model::create($data);
    }

    public function createMany(array $data)
    {
        return $this->model::createMany($data);
    }

    public function save($opject)
    {
        return $this->model::save($opject);
    }
    public function saveMany($opjects)
    {
        return $this->model::saveMany($opjects);
    }

    public function find(int $id)
    {
        return $this->model::find($id);
    }
    public function findMany(array $ids)
    {
        return $this->model::findMany($ids);
    }

    public function findBy($field,$value,$single = true)
    {
        if($single){
            return $this->model::where($field, '=', $value)->first();
        }
        return $this->model::where($field, '=', $value);
    }

    public function delete(int $id)
    {
        return $this->model::destroy($id);
    }

    public function truncate()
    {
        return $this->model::truncate();
    }

    public function all()
    {
        return $this->model::all();
    }

    public function paginate(int $per_page,$page = 1)
    {
        return $this->model::paginate($per_page); // url?param1=123&page=3
    }

    public function update($id,$data){
        $item = $this->find($id);
        if($item){
            $result = $item->update($data);
            if($result){
                return $item;
            }
        }
        return false;
    }

    public function random($num)
    {
        return $this->model::inRandomOrder()->take($num)->get();
    }

}