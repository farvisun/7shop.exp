<?php
namespace App\Services\Auth;

use App\Repositories\UserRepo;

class Auth{
    const loginColumn = 'email';
    const passwordColumn = 'password';

    public static function create_user($userData)
    {
        if (self::userExists($userData[self::loginColumn])) {
            return false;      // user with this email already exists !
        }
        // check $userData here
        $userData[self::passwordColumn] = password_hash($userData[self::passwordColumn], PASSWORD_BCRYPT);
        $repo = new UserRepo();
        return $repo->create($userData);
    }

    public static function current_user()
    {
        if(self::user_logged_in()){
            $repo = new UserRepo();
            return $repo->findBy(self::loginColumn, self::current_user_email());
        }
        return null;
    }

    public static function userExists($email)
    {
        $repo = new UserRepo();
        return !is_null($repo->findBy(self::loginColumn, $email));
    }

    public static function login($email,$password)
    {
        if (!self::userExists($email)) {
            return false;
        }
        $repo = new UserRepo();
        $user = $repo->findBy(self::loginColumn, $email);

        if (password_verify($password, $user->password)) {
            $_SESSION['login'] = $email;
            return true;
        }
        return false;
    }

    public static function user_logged_in()
    {
        if (empty($_SESSION['login'])) {
            return false;
        }
        return true;
    }

    public static function current_user_email()
    {
        if (self::user_logged_in()) {
            return $_SESSION['login'];
        }
        return null;
    }

    public static function logout()
    {
        $_SESSION['login'] = '';
        unset($_SESSION['login']);
    }

}