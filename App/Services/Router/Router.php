<?php
namespace App\Services\Router;

use App\Core\Request;
use App\Services\View\View;

class Router{
	private static $routes ;
	private static $baseController = "App\Controllers\\";
	private static $baseMiddleware = "App\Middleware\\";
	public static function register() {
		self::$routes = include_once ROUTES."web.php";
		$currentRoute = static::getCurrentRoute();
		if(self::isRouteDefined($currentRoute)){
			if(!in_array(strtolower($_SERVER['REQUEST_METHOD']),self::getRouteMethods($currentRoute))){
				echo "invalid request method.";
				exit;
			}
			list($controller,$method) = explode('@',static::getRouteTarget($currentRoute));
			$controllerClass = self::$baseController.$controller;
			$controllerInstance = new $controllerClass;
			if(method_exists($controllerInstance,$method)){
				$request = new Request();
				$middlewareClass = self::$baseMiddleware.self::getRouteMiddleware($currentRoute);
				if(class_exists($middlewareClass)){
					$middlewareInstance = new $middlewareClass;
					$middlewareInstance->handle($request);
				}

				$controllerInstance->$method($request);
			}else{
				echo "invalid operation";
			}

		}else{
			View::load('errors.404');
		}
		// get Route target
		// call middleware
		// new TargetController() and call the method
		exit;
	}

	public static function getCurrentRoute() {
		return strtok(strtolower($_SERVER['REQUEST_URI']),"?");
	}

	public static function isRouteDefined($key) {
		return array_key_exists($key,static::getRoutes());
	}

	public static function getRoutes() {
		return self::$routes;
	}

	public static function getRouteMethods($key) {
		$routes = static::getRoutes();
		$targetStr =  $routes[$key]['method'];
		return explode('|',$targetStr);
	}

	public static function getRouteTarget($key) {
		$routes = static::getRoutes();
		return  $routes[$key]['target'];
	}
	public static function getRouteMiddleware($key) {
		$routes = static::getRoutes();
		return $routes[$key]['middleware'] ?? null;
	}


}