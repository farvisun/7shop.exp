<div class="page-wrapper ssWrap">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card p-30">
                    <form action="<?= siteUrl("panel/media/save") ?>" method="post" enctype="multipart/form-data">
                        <?= csrf_field(); ?>
                        <input type="hidden" name="csrf" value="5a78b6aa22d9cad03c2fe3669de73250">
                        <div class="form-group">
                            <label>محصول</label>
                            <select name="product_id" class="form-control">
                                <?php foreach ($products as $product): ?>
                                <option value="<?= $product->id ?>"><?= $product->title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>نوع فایل</label>
                            <select name="type" class="form-control">
                                <option value="product_thumbnail">تصویر بند انگشتی (270x335)</option>
                                <option value="product_image">تصویر محصول</option>
                                <option value="product_3d_image">تصویر سه بعدی</option>
                                <option value="product_video">ویدیوی محصول</option>
                                <option value="product_slide">اسلاید صفحه نخست</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>آپلود فایل</label>
                            <input type="file" name="media" class="form-control" style="height: 55px;">
                        </div>
                        <button type="submit" class="btn btn-info btn-flat btn-addon m-b-10">افزدن</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
