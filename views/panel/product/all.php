<div class="page-wrapper ssWrap ss-dashboard">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>محصولات</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>دسته بندی</th>
                                    <th>عنوان</th>
                                    <th>قیمت</th>
                                    <th>موجودی</th>
                                    <th>عملیات</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($products as $product): ?>
                                    <tr>
                                        <td><?= $product->category_title ?></td>
                                        <td><?= $product->title ?></td>
                                        <td><?= $product->price ?></td>
                                        <td><?= $product->quantity ?></td>
                                        <td>
                                            <a href="<?= siteUrl('panel/products/delete?product=' . $product->id) ?>"><i class="fa fa-close"></i></a>
                                            <a href="<?= siteUrl('panel/products/edit?product=' . $product->id) ?>"><i class="fa fa-edit"></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
