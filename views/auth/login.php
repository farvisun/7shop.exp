<html>
<head>

<link href="<?= asset('css', 'lib/bootstrap/bootstrap-rtl.min.css') ?>" rel="stylesheet">
<link href="<?= asset('css', 'helper.css') ?>" rel="stylesheet">
<link href="<?= asset('css', 'fonts.css') ?>" rel="stylesheet">
<link href="<?= asset('css', 'style.css') ?>" rel="stylesheet">
    <style>
        body, html {
            height: 100%;
            background-repeat: no-repeat;
            background-image: linear-gradient(rgb(104, 145, 162), rgb(12, 97, 33));
        }

        .container.logincontainer {
            padding-top: 100px;
        }
    </style>
</head>
<body>
<div class="container logincontainer">
    <div class="card card-container">
        <img id="profile-img" class="profile-img-card" src="<?= asset('img', 'login.png') ?>"/>
        <p id="profile-name" class="profile-name-card"></p>
        <form class="form-signin" method="post" action="<?= siteUrl('auth/dologin') ?>">
            <?= csrf_field(); ?>
            <span id="reauth-email" class="reauth-email"></span>
            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">ورود</button>
        </form><!-- /form -->
        <div class="text-center"><a href="<?= siteUrl('auth/register') ?>">عضویت در سایت</a></div>
        <div class="text-center"><a href="<?= siteUrl() ?>">بازگشت به صفحه اصلی</a></div>

    </div><!-- /card-container -->
</div><!-- /container -->
<?php \App\Services\Flash\FlashMessage::show_messages(); ?>

<!--Custom JavaScript -->
<script src="<?= asset('js', 'lib/jquery/jquery.min.js') ?>"></script>
<script src="<?= asset('js', 'custom.min.js') ?>"></script>
</body>
</html>