<?php
// dont user helpers , use Utility Instead !
function getActualUrl(){
	$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
	return $actual_link;
}

function asset($type,$path = ''){
    $whitelist = ['js','css', 'img', 'vendor', 'fonts'];
    if(!in_array($type,$whitelist))
        return "invalid_type";
    $type_url = BASE_URL."assets/$type/";
    $file_url = "{$type_url}{$path}";
    return $file_url;
}

function siteUrl($route = ''){
    return BASE_URL.$route;
}

function csrf_field(){
    $token = bin2hex(random_bytes(16));
    $_SESSION['csrf_token'] = $token;
    return "<input type='hidden' name='csrf' value='$token' >";
}

function getFormField($attr){

    switch ($attr->type){
        case 'tinytext':
            return "<input type='text' name='attr[$attr->id]' class='form-control' value='' placeholder='$attr->title'>";
        case 'bigtext':
            return "<textarea name='attr[$attr->id]' class='form-control' placeholder='$attr->title'></textarea>";
        case 'float':
            return "<input type='number'  step='0.01' name='attr[$attr->id]' class='form-control' value='' placeholder='$attr->title'>";
        case 'integer':
            return "<input type='number' name='attr[$attr->id]' class='form-control' value='' placeholder='$attr->title'>";
        default :
            return "<input type='text' name='attr[$attr->id]' class='form-control' value='' placeholder='$attr->title'>";
    }
}

