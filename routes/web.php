<?php
return [
    // Home Routes
    '/' => [
        'method' => 'get',
        'target' => 'HomeController@index',
    ],
    '/home' => [
        'method' => 'get',
        'target' => 'HomeController@index',
    ],

    // Product
    '/product' => [
        'method' => 'get|post',
        'target' => 'ProductController@index',
    ],
    // Order
    '/order/cart' => [
        'method' => 'get',
        'target' => 'OrderController@cart',
    ],
    '/order/verify' => [
        'method' => 'post',
        'target' => 'OrderController@verify',
        // 'middleware' => 'notLoginRedirect'
    ],

    // Panel Dashboard Routes
    '/panel' => [
        'method' => 'get',
        'target' => 'Panel\DashboardController@index',
    ],
    '/panel/dashboard' => [
        'method' => 'get',
        'target' => 'Panel\DashboardController@index',
    ],

    // Product Routes
    '/panel/products/all' => [
        'method' => 'get',
        'target' => 'Panel\ProductController@all',
    ],
    '/panel/product/add' => [
        'method' => 'get',
        'target' => 'Panel\ProductController@add',
    ],
    '/panel/product/save' => [
        'method' => 'post',
        'target' => 'Panel\ProductController@save',
    ],

    // Media Routes
    '/panel/media' => [
        'method' => 'get',
        'target' => 'Panel\MediaController@index',
    ],
    '/panel/media/add' => [
        'method' => 'get',
        'target' => 'Panel\MediaController@add',
    ],
    '/panel/media/save' => [
        'method' => 'post',
        'target' => 'Panel\MediaController@save',
    ],

    // Comment Routes
    '/panel/comments' => [
        'method' => 'get',
        'target' => 'Panel\CommentController@index',
    ],

    // Category and Attribute Routes
    '/panel/categories' => [
        'method' => 'get',
        'target' => 'Panel\CategoryController@index',
    ],
    '/panel/categories/create' => [
        'method' => 'post',
        'target' => 'Panel\CategoryController@create',
        'middleware' => 'SecurityMiddleware'
    ],
    '/panel/categories/attributes' => [
        'method' => 'get',
        'target' => 'Panel\CategoryController@attributes',
    ],
    '/panel/categories/modify-attributes' => [
        'method' => 'post',
        'target' => 'Panel\CategoryController@modifyAttributes',
    ],
    '/panel/attributes' => [
        'method' => 'get',
        'target' => 'Panel\AttributeController@index'
    ],
    '/panel/attributes/create' => [
        'method' => 'post',
        'target' => 'Panel\AttributeController@create',
        'middleware' => 'SecurityMiddleware'
    ],

    // Order Routes
    '/panel/orders' => [
        'method' => 'get',
        'target' => 'Panel\OrderController@index',
    ],

    // Shipment Routes
    '/panel/shipments' => [
        'method' => 'get',
        'target' => 'Panel\ShipmentController@index',
    ],

    // Payment Routes
    '/panel/payments' => [
        'method' => 'get',
        'target' => 'Panel\PaymentController@index',
    ],

    // User Routes
    '/panel/users' => [
        'method' => 'get',
        'target' => 'Panel\UserController@index',
    ],
    '/panel/users/add' => [
        'method' => 'get',
        'target' => 'Panel\UserController@add',
    ],
    '/panel/users/create' => [
        'method' => 'post',
        'target' => 'Panel\UserController@create',
    ],
    '/panel/user/profile' => [
        'method' => 'get',
        'target' => 'Panel\UserController@profile',
    ], '/panel/user/addresses' => [
        'method' => 'get',
        'target' => 'Panel\UserController@addresses',
    ],

    // Option Routes
    '/panel/options' => [
        'method' => 'get',
        'target' => 'Panel\OptionController@index',
    ],
    '/panel/options/add' => [
        'method' => 'get',
        'target' => 'Panel\OptionController@add',
    ],
    '/panel/options/create' => [
        'method' => 'post',
        'target' => 'Panel\OptionController@create',
        'middleware' => 'SecurityMiddleware'
    ],

    // auth
    '/auth/login' => [
        'method' => 'get',
        'target' => 'AuthController@login',
    ],
    '/auth/dologin' => [
        'method' => 'post',
        'target' => 'AuthController@dologin',
        'middleware' => 'SecurityMiddleware'
    ],
    '/auth/logout' => [
        'method' => 'get',
        'target' => 'AuthController@logout',
    ],
    '/auth/register' => [
        'method' => 'get',
        'target' => 'AuthController@register',
    ],
    '/auth/doregister' => [
        'method' => 'post',
        'target' => 'AuthController@doregister',
        'middleware' => 'SecurityMiddleware'
    ],
];
